document.addEventListener('DOMContentLoaded', function () {
    class Task {
        constructor(text, completed = false) {
            this.text = text;
            this.completed = completed;
        }
    }

    class TaskList {
        constructor() {
            this.tasks = [];
        }

        addTask(task) {
            this.tasks.push(task);
        }

        removeTask(task) {
            this.tasks = this.tasks.filter(t => t !== task);
        }

        getTasks(completed) {
            return this.tasks.filter(task => task.completed === completed);
        }
    }

    class Storage {
        static saveTasks(tasks) {
            localStorage.setItem('tasks', JSON.stringify(tasks));
        }

        static loadTasks() {
            const tasks = JSON.parse(localStorage.getItem('tasks')) || [];
            return tasks.map(taskData => new Task(taskData.text, taskData.completed));
        }
    }

    class UI {
        constructor(taskManager) {
            this.taskManager = taskManager;

            this.taskInput = document.getElementById('taskInput');
            this.addTaskButton = document.getElementById('addTaskButton');
            this.tasksContainer = document.getElementById('tasksContainer');
            this.completedTasksContainer = document.getElementById('completedTasksContainer');
            this.tasksCount = document.getElementById('tasksCount');
            this.completedTasksCount = document.getElementById('completedTasksCount');

            this.addTaskButton.addEventListener('click', () => this.taskManager.handleAddTask());

            this.updateTaskCounts();
        }

        updateTaskCounts() {
            this.tasksCount.textContent = this.formatCount(this.tasksContainer.querySelectorAll('.task').length);
            this.completedTasksCount.textContent = this.formatCount(this.completedTasksContainer.querySelectorAll('.task').length);
        }

        formatCount(count) {
            return count > 0 ? ` - ${count}` : '';
        }

        addTaskToUI(task) {
            const taskElement = this.createTaskElement(task);
            this.appendTaskElement(taskElement, task.completed);
            this.updateTaskCounts();
        }

        createTaskElement(task) {
            const taskElement = document.createElement('div');
            taskElement.classList.add('task');
            if (task.completed) {
                taskElement.classList.add('completed');
            }

            const taskText = document.createElement('span');
            taskText.classList.add('task-text');
            taskText.textContent = task.text;
            taskElement.appendChild(taskText);

            const controlsContainer = this.createControlsContainer(task, taskElement);
            taskElement.appendChild(controlsContainer);

            return taskElement;
        }

        createControlsContainer(task, taskElement) {
            const controlsContainer = document.createElement('div');
            controlsContainer.classList.add('task-controls');

            const checkbox = document.createElement('input');
            checkbox.type = 'checkbox';
            checkbox.checked = task.completed;
            const label = document.createElement('label');
            label.appendChild(checkbox);
            controlsContainer.appendChild(label);

            checkbox.addEventListener('change', () => {
                task.completed = checkbox.checked;
                if (checkbox.checked) {
                    taskElement.classList.add('completed');
                    this.completedTasksContainer.appendChild(taskElement);
                } else {
                    taskElement.classList.remove('completed');
                    this.tasksContainer.appendChild(taskElement);
                }
                this.updateTaskCounts();
                Storage.saveTasks(this.taskManager.taskList.tasks);
            });

            const deleteButton = document.createElement('button');
            deleteButton.addEventListener('click', () => {
                this.taskManager.taskList.removeTask(task);
                taskElement.remove();
                this.updateTaskCounts();
                Storage.saveTasks(this.taskManager.taskList.tasks);
            });
            controlsContainer.appendChild(deleteButton);

            return controlsContainer;
        }

        appendTaskElement(taskElement, completed) {
            if (completed) {
                this.completedTasksContainer.appendChild(taskElement);
            } else {
                this.tasksContainer.appendChild(taskElement);
            }
        }

        clearTasks() {
            this.tasksContainer.innerHTML = '';
            this.completedTasksContainer.innerHTML = '';
        }
    }

    class TaskManager {
        constructor() {
            this.taskList = new TaskList();
            this.ui = new UI(this);

            this.loadTasksFromStorage();
        }

        handleAddTask() {
            const taskText = this.ui.taskInput.value.trim();
            if (taskText !== '') {
                const task = new Task(taskText);
                this.taskList.addTask(task);
                this.ui.addTaskToUI(task);
                this.ui.taskInput.value = '';
                Storage.saveTasks(this.taskList.tasks);
            }
        }

        loadTasksFromStorage() {
            const tasks = Storage.loadTasks();
            tasks.forEach(task => {
                this.taskList.addTask(task);
                this.ui.addTaskToUI(task);
            });
        }
    }

    new TaskManager();
});
